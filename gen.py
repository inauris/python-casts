def gen_countdown(n):
    while n != 0:
        n -= 1
        yield n


g = gen_countdown(4)
print(next(g))
# print(next(g))
# print(next(g))
# print(next(g))

# for i in gen_countdown(5):
#     print(i)
