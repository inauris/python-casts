# ---------------------------- PART 1 ----------------------------
# Syntax

# jack = {
#     'name': 'jack',
#     'car': 'bmw'
# }
#
# john = {
#     'name': 'john',
# }
# users = [jack, john, jack, john]  # список словарей

'''Standart cycle for'''
# cars = []
# for person in users:
#     cars.append(person['car'])
# print(cars)

'''Alternative list comprehension'''
# cars = [person.get('car', '') for person in users]
# print(cars)

'''Antipattern of list comprehension.
Don't use ['key'], only .get('key', 'default value') '''
# cars = [person['car'] for person in users]
# print(cars)


# ---------------------------- PART 2 ----------------------------
# Filering
'''List comprehension'''
names = ['Jack', 'John', 'Oleg', 'Ula']

new_names = [name for name in names if name.startswith('J')]
print(new_names)


'''Alternative standart cycle for'''
# new_names = []
# for name in names:
#     if name.startswith('J'):
#         new_names.append(name)
# print(new_names)
