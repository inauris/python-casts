from time import monotonic

# ---------------------------- DECORATOR 1 ----------------------------
# Without return result

# def get_even_for():
#     list_of_even = []
#     for number in range(10**4):
#         if number % 2 == 0:
#             list_of_even.append(number)
#     return list_of_even
#
#
# def get_even_with_list_comprehencion():
#     list_of_even = [number for number in range(10**4) if number % 2 == 0]
#     return list_of_even
#
#
# def time_decorator(func):
#     start = monotonic()
#     func()
#     print(f'{func.__name__}:', monotonic() - start)
#
#
# time_decorator(get_even_for)
# time_decorator(get_even_with_list_comprehencion)


# ---------------------------- DECORATOR 2 ----------------------------

def timeit(func):
    def wrapper(*args, **kwargs):
        start = monotonic()
        result = func(*args, **kwargs)
        print(f'{func.__name__}:', monotonic() - start)
        return result
    return wrapper


@timeit
def get_even_for(n):
    list_of_even = []
    for number in range(n):
        if number % 2 == 0:
            list_of_even.append(number)
    return list_of_even


@timeit
def get_even_with_list_comprehencion(n):
    list_of_even = [number for number in range(n) if number % 2 == 0]
    return list_of_even


l1 = get_even_for(100)
l2 = get_even_with_list_comprehencion(1000)
# print(l1)
# print(l2)


# ---------------------------- DECORATOR 3 ----------------------------

# def timeit(arg):
#     print(arg)
#
#     def outer(func):
#         def wrapper(*args, **kwargs):
#             start = monotonic()
#             result = func(*args, **kwargs)
#             print(f'{func.__name__}:', monotonic() - start)
#             return result
#         return wrapper
#     return outer
#
#
# @timeit('name')
# def get_even_for(n):
#     list_of_even = []
#     for number in range(n):
#         if number % 2 == 0:
#             list_of_even.append(number)
#     return list_of_even
#
#
# @timeit('name')
# def get_even_with_list_comprehencion(n):
#     list_of_even = [number for number in range(n) if number % 2 == 0]
#     return list_of_even
#
#
# l1 = get_even_for(100)
# l2 = get_even_with_list_comprehencion(1000)
# print(l1)
# print(l2)
#

'''
Decorator without "@"

        @timeit('arg')
             ||
l1 = timeit('arg')(func)(10)
'''
